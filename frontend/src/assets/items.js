import pic1 from './images/1.png'
import pic3 from './images/3.png'
import pic2 from './images/2.png'
import pic4 from './images/4.png'
import pic5 from './images/5.jpg'
import pic6 from './images/6.jpg'
import pic7 from './images/7.jpg'
import pic8 from './images/8.jpg'
import pic9 from './images/9.png'

const percentageDiscount = {
  type: 'percDiscount',
  amount: 20,
  text: '20%',
};

const multipleFree = {
  type: 'multipleFree',
  every: 3,
  free: 1,
  text: '3 per 2'
}

const tshirts = [
  { id: 1, price: 18.90, discount: percentageDiscount, name: 'Shirt 1', imageUrl: pic1 },
  { id: 2, price: 16.90, name: 'Shirt 2', imageUrl: pic2 },
  { id: 3, price: 14.90, discount: multipleFree, name: 'Shirt 3', imageUrl: pic3 },
  { id: 4, price: 10.90, name: 'Shirt 4', imageUrl: pic4 },
  { id: 5, price: 13, name: 'Shirt 5', imageUrl: pic5 },
  { id: 6, price: 11.50, name: 'Shirt 6', imageUrl: pic6 },
  { id: 7, price: 12.90, name: 'Shirt 7', imageUrl: pic7 },
  { id: 8, price: 14.90, name: 'Shirt 8', imageUrl: pic8 },
  { id: 9, price: 16.90, name: 'Shirt 9', imageUrl: pic9 },
]

export default tshirts;
