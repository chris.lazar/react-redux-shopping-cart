import React from 'react';

const styles = {
  display: 'flex',
}

export default ({ children }) => <div style={styles}>{children}</div>
