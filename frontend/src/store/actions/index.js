import { ADD_TO_CART } from '../types';

export const addToCart = shirtId => {
    return {
        type: ADD_TO_CART,
        data: shirtId
    }
}
