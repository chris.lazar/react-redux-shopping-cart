export const percentageDiscount = {
    type: 'percDiscount',
    amount: 20,
    text: '20%',
};

export const multipleFree = {
    type: 'multipleFree',
    every: 3,
    free: 1,
    text: '3 per 2'
};
