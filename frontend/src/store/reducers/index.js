import { combineReducers } from 'redux';
import shoppingCart from './shoppingCart';
import { items } from './items';

const store = combineReducers({
    shoppingCart,
    items
})

export default store;