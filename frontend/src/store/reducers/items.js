import { percentageDiscount, multipleFree } from '../discounts';

import pic1 from '../../assets/images/1.png'
import pic3 from '../../assets/images/3.png'
import pic2 from '../../assets/images/2.png'
import pic4 from '../../assets/images/4.png'
import pic5 from '../../assets/images/5.jpg'
import pic6 from '../../assets/images/6.jpg'
import pic7 from '../../assets/images/7.jpg'
import pic8 from '../../assets/images/8.jpg'
import pic9 from '../../assets/images/9.png'


export const shirts = [
    { id: 1, price: 18.90, discount: percentageDiscount, name: 'Shirt 1', imageUrl: pic1 },
    { id: 2, price: 16.90, name: 'Shirt 2', imageUrl: pic2 },
    { id: 3, price: 14.90, discount: multipleFree, name: 'Shirt 3', imageUrl: pic3 },
    { id: 4, price: 10.90, name: 'Shirt 4', imageUrl: pic4 },
    { id: 5, price: 13, name: 'Shirt 5', imageUrl: pic5 },
    { id: 6, price: 11.50, name: 'Shirt 6', imageUrl: pic6 },
    { id: 7, price: 12.90, name: 'Shirt 7', imageUrl: pic7 },
    { id: 8, price: 14.90, name: 'Shirt 8', imageUrl: pic8 },
    { id: 9, price: 16.90, name: 'Shirt 9', imageUrl: pic9 },
]


export const items = (state = shirts, action) => {
    return state
}
