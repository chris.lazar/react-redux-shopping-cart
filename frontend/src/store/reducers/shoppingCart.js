import { ADD_TO_CART } from '../types';
import { shirts } from './items';

const initialState = {};

const shoppingCart = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TO_CART: {
            const selectedItemIndex = shirts.findIndex(item => item.id === action.data);
            const shirt = shirts[selectedItemIndex];
            let newShirt;
            if (state[action.data]) {
                newShirt = { ...state[action.data] }
                newShirt.quantity += 1;
            } else {
                newShirt = {
                    ...shirt,
                    quantity: 1
                }
            }

            const newState = {
                ...state,
                [action.data]: newShirt
            }
            return newState;
        }

        default:
            return state
    }
}

export default shoppingCart;