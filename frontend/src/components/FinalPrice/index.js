import React from 'react';

import calculatePrice from '../../store/calculatePrice';

export default ({ item }) => <span>{`CH ${calculatePrice(item)}`}</span>
