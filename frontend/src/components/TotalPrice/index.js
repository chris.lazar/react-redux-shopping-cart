import React from 'react';

import calculatePrice from '../../store/calculatePrice';

const calculateTotal = (items) => {
  return items.map(item => {
    return item
  }).reduce((acc, item) => {
    return acc + calculatePrice(item)
  }, 0)
}

export default ({ items }) => {
  return <span>{`CH ${(Math.round(calculateTotal(items) * 100) / 100)}`}</span>
}

