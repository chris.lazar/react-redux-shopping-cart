import React from 'react';
import { IconButton } from 'material-ui';
import AddShoppingCart from 'material-ui/svg-icons/action/add-shopping-cart';


export default ({ handleAddToCart }) =>
  <IconButton onClick={handleAddToCart}>
    <AddShoppingCart color="white" />
  </IconButton>
