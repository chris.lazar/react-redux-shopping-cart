import React, { Component } from 'react';
import { GridTile } from 'material-ui';
import { connect } from 'react-redux';
import { addToCart } from '../../store/actions'

import AddToCart from '../../components/AddToCart';

class ItemList extends Component {

  handleAddToCart = () => {
    this.props.dispatch(addToCart(this.props.item.id))
  }

  render() {
    const { item } = this.props;

    return (
      <GridTile
        key={item.id}
        title={item.name}
        titleBackground="linear-gradient(to top, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.3) 70%,rgba(0,0,0,0) 100%)"
        subtitle={
          <span>
            <span>{`CH ${item.price}`}</span>
            {item.discount && <span>{` - ${item.discount.text}`}</span>}
          </span>
        }
        actionIcon={<AddToCart handleAddToCart={this.handleAddToCart} />}
      >
        <img src={item.imageUrl} alt={item.name} />
      </GridTile>
    )
  }
}

export default connect()(ItemList);