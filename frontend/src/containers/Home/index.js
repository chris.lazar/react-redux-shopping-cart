import { AppBar } from 'material-ui';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import Container from '../../hoc/container';
import ItemsList from '../ItemsList';
import ShoppingCart from '../ShoppingCart';

class Home extends Component {

  cartItems() {
    return Object.values(this.props.items).length;
  }

  render() {
    const cartItems = this.cartItems();

    return (
      <div>
        <AppBar title="Cool T-Shirts!" />
        <Container>
          <ItemsList />
          {cartItems ? <ShoppingCart /> : <h5>No Items yet!</h5>}
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    items: state.shoppingCart
  }
}

export default connect(mapStateToProps)(Home);
