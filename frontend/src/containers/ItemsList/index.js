import React, { Component } from 'react'
import { GridList } from 'material-ui';
import { connect } from 'react-redux';

import ItemList from '../ItemList';

const styles = {
  root: {
    width: '600px',
    marginTop: '50px',
  }
}

class ItemsList extends Component {
  render() {
    return (
      <GridList
        cols={3}
        style={styles.root}
        cellHeight={200}
      >
        {
          this.props.items.map(item => <ItemList key={item.id} item={item} />)
        }
      </GridList>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    items: state.items
  }
}

export default connect(mapStateToProps)(ItemsList);
